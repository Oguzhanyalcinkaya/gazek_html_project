const { dest } = require("gulp");
let gulp = require("gulp"),
    imagemin = require("gulp-imagemin"),
    concat = require("gulp-concat"),
    minifyCSS = require("gulp-clean-css"),
    uglify = require("gulp-uglify"),
    nunjucksRender = require("gulp-nunjucks-render"),
    glob = require("glob"),
    htmlmin = require("gulp-htmlmin");

//HTML

gulp.task("css", () => {
    return gulp.src("./HTML/assets/css/**/*.css")
    .pipe(concat("all.css"))
    .pipe(minifyCSS())
    .pipe(gulp.dest("./dist/css/"))
});

gulp.task("tema", () => {
    return gulp.src("./HTML/assets/color-themes/*.css")
    .pipe(minifyCSS())
    .pipe(gulp.dest("./dist/tema/"))
});

gulp.task("images", () => {
    return gulp.src(["./HTML/assets/**/*.png", "./HTML/assets/**/*.jpg", "./HTML/assets/**/*.jpeg", "./HTML/assets/**/*.gif", "./HTML/assets/images/**/*.svg"])
    .pipe(imagemin())
    .pipe(gulp.dest("./dist/images/"));
});

gulp.task("fonts", () => {
    return gulp.src(["./HTML/assets/fonts/**/*.eot", "./HTML/assets/fonts/**/*.svg", "./HTML/assets/fonts/**/*.ttf", "./HTML/assets/fonts/**/*.woff", "./HTML/assets/fonts/**/*.woff2"])
    .pipe(gulp.dest("./dist/fonts/"));
});

gulp.task("js", () => {
    return gulp.src(["./HTML/assets/js/jquery.js", "./HTML/assets/js/popper.min.js", "./HTML/assets/js/bootstrap.min.js","./HTML/assets/js/jquery.mCustomScrollbar.concat.min.js",
    "./HTML/assets/js/jquery.fancybox.js","./HTML/assets/js/appear.js","./HTML/assets/js/isotope.js","./HTML/assets/js/parallax.min.js","./HTML/assets/js/tilt.jquery.min.js","./HTML/assets/js/jquery.paroller.min.js",
    "./HTML/assets/js/owl.js","./HTML/assets/js/validate.js","./HTML/assets/js/wow.js", "./HTML/assets/js/jquery-ui.js","./HTML/assets/js/script.js","./HTML/assets/js/color-settings.js"])
    .pipe(concat("script.js"))
    .pipe(uglify())
    .pipe(gulp.dest("./dist/js/"));
});

gulp.task("php", () => {
    return gulp.src("./HTML/**/*.php")
    .pipe(dest("./dist/php"))
});

gulp.task("nunjucks", () => {
    var thenunjucksPath = ["./HTML/nunjucks/*"];
    return gulp.src("./HTML/**/*.html")
    .pipe(nunjucksRender({
        path : thenunjucksPath.reduce((param1, param2) => {
            return param1.concat(glob.sync(param2));
        }, []),
    }))
    .pipe(gulp.dest("./dist/"))
});

//Gazek-doc
gulp.task("html-gazek", () => {
    return gulp.src("./Gazek-doc/**/*.html")
    // .pipe(htmlmin)
    .pipe(gulp.dest("./dist-Gazek-doc/"));
});

gulp.task("css-gazek", () => {
    return gulp.src("./Gazek-doc/**/*.css")
    .pipe(concat("style.css"))
    .pipe(minifyCSS())
    .pipe(gulp.dest("./dist-Gazek-doc/css/"));
});

gulp.task("js-gazek", () => {
    return gulp.src(["./Gazek-doc/js/jquery.js", "./Gazek-doc/js/bootstrap.min.js","./Gazek-doc/js/jquery.nav.js","./Gazek-doc/js/jquery.scrollTo.js","./Gazek-doc/js/scrollbar.js","./Gazek-doc/js/script.js"])
    .pipe(concat("script.js"))
    .pipe(uglify())
    .pipe(gulp.dest("./dist-Gazek-doc/js/"));
});

gulp.task("image-gazek", () => {
    return gulp.src("./Gazek-doc/images/*")
    .pipe(imagemin())
    .pipe(gulp.dest("./dist-Gazek-doc/images/"))
});

gulp.task("font-gazek", () => {
    return gulp.src("./Gazek-doc/fonts/*")
    .pipe(gulp.dest("./dist-Gazek-doc/fonts/"))
});

gulp.task("watch", () => {
    gulp.watch("./HTML/**/*.html", gulp.series("nunjucks"));
    gulp.watch("./HTML/assets/css/**/*.css", gulp.series("css"))
    gulp.watch("./HTML/assets/color-themes/*.css", gulp.series("tema"))
    gulp.watch(["./HTML/assets/**/*.png", "./HTML/assets/**/*.jpg", "./HTML/assets/**/*.jpeg", "./HTML/assets/**/*.gif", "./HTML/assets/images/**/*.svg"], gulp.series("images"))
    gulp.watch(["./HTML/assets/fonts/**/*.eot", "./HTML/assets/fonts/**/*.svg", "./HTML/assets/fonts/**/*.ttf", "./HTML/assets/fonts/**/*.woff", "./HTML/assets/fonts/**/*.woff2"], gulp.series("fonts"))
    gulp.watch("./HTML/assets/js/**/*.js", gulp.series("js"))
    gulp.watch("./HTML/assets/**/*.php", gulp.series("php"))

    gulp.watch("./Gazek-doc/**/*.html", gulp.series("html-gazek"))
    gulp.watch("./Gazek-doc/**/*.css", gulp.series("css-gazek"))
    gulp.watch("./Gazek-doc/**/*.js", gulp.series("js-gazek"))
    gulp.watch("./Gazek-doc/images/*", gulp.series("image-gazek"))
    gulp.watch("./Gazek-doc/fonts/*", gulp.series("font-gazek"))
})

gulp.task("default", gulp.series(["nunjucks", "css", "tema", "images", "fonts", "js", "php",
        "html-gazek", "css-gazek", "js-gazek","image-gazek","font-gazek", "watch"]));